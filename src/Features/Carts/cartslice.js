export const cartData = {
  showAddToCartModal: false,
  cartProduct: [],
  showDeleteModal: false,
};

export function CartReducer(state = {}, action) {
  if (action.type == "addToCartModal") {
    return {
      ...state,
      showAddToCartModal: action.payload.showAddToCartModal,
    };
  }
  if (action.type == "deleteFromCartModal") {
    return {
      ...state,
      showDeleteModal: action.payload.showDeleteModal,
    };
  }
  if (action.type == "add-Cart-data") {
    return {
      ...state,
      cartProduct: action.payload.cartProduct,
    };
  }

  if (action.type == "deleteFromCart") {
    return {
      ...state,
      cartProduct: action.payload.cartProduct,
    };
  }

  return state;
}

export function getCartModal(state) {
  return state.cartdata.showAddToCartModal;
}

export function getDeleteCartModal(state) {
  return state.cartdata.showDeleteModal;
}

export function returnData(state) {
  return state.cartdata.cartProduct;
}

export function openModal(newData) {
  return {
    type: "addToCartModal",
    payload: {
      showAddToCartModal: newData,
    },
  };
}

export function openDeleteModal(newData) {
  return {
    type: "deleteFromCartModal",
    payload: {
      showDeleteModal: newData,
    },
  };
}

export function editCart(data) {
  return {
    type: "add-Cart-data",
    payload: {
      cartProduct: data,
    },
  };
}

export function deleteCart(data) {
  return {
    type: "deleteFromCart",
    payload: {
      cartProduct: data,
    },
  };
}
export function showCartModal() {
  return (dispatch, getstate) => {
    const newData = getstate().cartdata.showAddToCartModal;
    return dispatch(openModal(!newData));
  };
}

export function showDeleteModal() {
  return (dispatch, getstate) => {
    const newData = getstate().cartdata.showDeleteModal;
    return dispatch(openDeleteModal(!newData));
  };
}
export function addToCart(product) {
  return (dispatch, getstate) => {
    const cartData = getstate().cartdata.cartProduct;
    const newData = cartData ? [...cartData, product] : product;
    localStorage.cartData = JSON.stringify(newData);
    return dispatch(editCart(newData));
  };
}

export function deleteFromCart(product) {
  return (dispatch, getstate) => {
    const newData = getstate().cartdata.cartProduct.filter(
      (item) => item.id !== product.id
    );
    localStorage.cartData = JSON.stringify(newData);
    return dispatch(deleteCart(newData));
  };
}
