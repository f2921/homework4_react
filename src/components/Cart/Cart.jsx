import "./Cart.scss";
import { BsStarFill } from "react-icons/bs";

export default function Cart({ cartData, showDeleteModal, checkItemData }) {
  return (
    <div className="item-list">
      {cartData.length ? (
        cartData.map((product) => {
          return (
            <div className="item" key={product.id}>
              <span
                className="close"
                onClick={() => {
                  showDeleteModal();
                  checkItemData(product);
                }}
              >
                &times;
              </span>
              <div className="item__content">
                <div className="item__content--img-block">
                  <img src={product.image} />
                </div>
                <div className="item__content--info-block">
                  <h3 className="item-title">{product.title}</h3>
                  <p className="item-price">{product.price} грн</p>
                  <p>Артикул: {product.id}</p>
                </div>
              </div>
            </div>
          );
        })
      ) : (
        <h1>Product Count 0</h1>
      )}
    </div>
  );
}
