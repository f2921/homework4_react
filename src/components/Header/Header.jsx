import React from "react";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import "./Header.scss";
import { NavLink, Outlet } from "react-router-dom";
import { BsStarFill, BsFillCartFill } from "react-icons/bs";

function Header(props) {
  return (
    <>
      <Navbar bg="primary" variant="dark">
        <Container>
          <Navbar.Brand href="#home">
            <img
              className="logo1"
              src={process.env.PUBLIC_URL + "/weblogo1.png"}
            />
          </Navbar.Brand>
          <Nav className="me-auto">
            <NavLink to="/">home</NavLink>
            <NavLink to="favorites">Favorite</NavLink>
            <NavLink to="cart">Shopping</NavLink>
            <Nav.Link href="#favorite">
              <BsStarFill />
              <span>{props.favData.length}</span>
            </Nav.Link>
            <Nav.Link href="#cart">
              <BsFillCartFill />
              <span>{props.cartData.length}</span>
            </Nav.Link>
          </Nav>
        </Container>
      </Navbar>
      <Outlet />
    </>
  );
}
export default Header;
